package com.company;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        ej5();

    }

    public static void ej1(){

        Scanner scanner = new Scanner(System.in);

        LocalDate fechaActual = LocalDate.now();
        LocalTime horaActual = LocalTime.now();

        System.out.println("Fecha actual: "+fechaActual);
        System.out.println("Hora actual: "+horaActual);

        System.out.println("Pulsa 1 para ver solo el año");
        System.out.println("Pulsa 2 para ver solo el mes");
        System.out.println("Pulsa 3 para ver solo el dia");

        int eleccion = scanner.nextInt();

        if (eleccion==1){
            System.out.println(fechaActual.getYear());
        }else if (eleccion==2){
            System.out.println(fechaActual.getMonth());
        }else if (eleccion==3){
            System.out.println(fechaActual.getDayOfMonth());
        }

    }

    public static void ej2(){

        Scanner scanner = new Scanner(System.in);

        System.out.println("Introduce una fecha para compararla con la actual");

        System.out.println("Introduce el dia");
        int day = scanner.nextInt();

        System.out.println("Introduce el mes");
        int month = scanner.nextInt();
        
        System.out.println("Introduce el año");
        int year = scanner.nextInt();

        LocalDate askedDate = LocalDate.of(year,month,day);
        LocalDate nowDate = LocalDate.now();

        boolean before = askedDate.isBefore(nowDate);
        boolean equal = askedDate.equals(nowDate);
        boolean after = askedDate.isAfter(nowDate);

        if (before){

            System.out.println("La fecha introducida es anterior a la fecha actual");

        }else if (equal){

            System.out.println("La fecha introducida es la misma que la actual");

        }else if (after){

            System.out.println("La fecha introducida es posterior a la fecha actual");

        }

    }

    public static void ej3(){

        Scanner scanner = new Scanner(System.in);

        LocalTime horaActual = LocalTime.now();
        LocalDate fechaActual = LocalDate.now();
        LocalDateTime fechaYhoraActual = LocalDateTime.now();


        System.out.println("La fecha y hora actual es: "+fechaYhoraActual);
        System.out.println("Que quieres sumarle");
        System.out.println("1-Sumar dias");
        System.out.println("2-Sumar horas");
        System.out.println("3-Sumar minutos");

        int eleccion = scanner.nextInt();

        if (eleccion==1){

            System.out.println("Cuantos dias quieres sumar");
            int diasAñadir = scanner.nextInt();
            LocalDateTime fechaYhoraAddedDays = fechaYhoraActual.plusDays(diasAñadir);
            System.out.println("La fecha y hora despues de añadirle "+diasAñadir+" dias es: "+fechaYhoraAddedDays);


        }else if (eleccion==2){

            System.out.println("Cuantas horas quieres sumar");
            int horasAñadir = scanner.nextInt();
            LocalDateTime fechaYhoraAddedHours = fechaYhoraActual.plusHours(horasAñadir);
            System.out.println("La fecha y hora despues de añadirle "+horasAñadir+" horas es: "+fechaYhoraAddedHours);

        }else if (eleccion==3){

            System.out.println("Cuantos minutos quieres sumar");
            int minutosAñadir = scanner.nextInt();
            LocalDateTime fechaYhoraAddedMinutes = fechaYhoraActual.plusMinutes(minutosAñadir);
            System.out.println("La fecha y hora despues de añadirle "+fechaYhoraAddedMinutes+" minutos es: "+fechaYhoraAddedMinutes);

        }

    }

    public static void ej4(){

        Scanner scanner = new Scanner(System.in);

        System.out.println("Introduce la fecha y hora a convertir en formato DD/MM/YYYY hh:mm:ss");

        System.out.println("Introduce el año");

        int year = scanner.nextInt();

        System.out.println("Introduce el mes");

        int month = scanner.nextInt();

        System.out.println("Introduce el dia");

        int day = scanner.nextInt();

        System.out.println("Introduce la hora");

        int hour = scanner.nextInt();

        System.out.println("Introduce los minutos");

        int minutes = scanner.nextInt();

        System.out.println("Introduce los segundos");

        int seconds = scanner.nextInt();

        LocalDateTime fechaYhora = LocalDateTime.of(year,month,day,hour,minutes,seconds);
        DateTimeFormatter formatoFechaHora = DateTimeFormatter.ofPattern("dd/MM/YYYY hh:mm:ss");
        System.out.println("Fecha y hora antes de cambiar de formato "+fechaYhora);
        System.out.println("La fecha y hora han cambiado de formato "+fechaYhora.format(formatoFechaHora));


    }

    public static void ej5(){

        Scanner scanner = new Scanner(System.in);

        System.out.println(ZoneId.getAvailableZoneIds());

        LocalDateTime localDateTime = LocalDateTime.now();

        System.out.println("Donde quieres ver la hora actual");
        System.out.println("1-Madrid");
        System.out.println("2-Africa");
        System.out.println("3-Londres");

        int eleccion = scanner.nextInt();

        LocalDateTime now = LocalDateTime.now();


        if (eleccion == 1) {

            LocalDateTime Madrid = LocalDateTime.now(ZoneId.of("Europe/Madrid"));
            System.out.println("Mostrando hora en Madrid");
            System.out.println(Madrid);

        }else if (eleccion == 2){

            LocalDateTime Cairo = LocalDateTime.now(ZoneId.of("Africa/Cairo"));

            System.out.println("Mostrando hora en el Cairo");
            System.out.println(Cairo);


        }else if (eleccion == 3){

            LocalDateTime Londres = LocalDateTime.now(ZoneId.of("Europe/London"));

            System.out.println("Mostrando hora en Londres");

            System.out.println(Londres);
        }



    }
}
