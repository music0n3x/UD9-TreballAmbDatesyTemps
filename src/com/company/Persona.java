package com.company;

import java.time.LocalDateTime;
import java.util.Date;


public class Persona {

    private String nombre;
    private String apellido;
    private String direccion;
    private Date cumpleaños;
    private String email;
    private String numeroTelefono;
    private LocalDateTime fechaCreacionCuenta;
    private String password;

    public Persona(String nombre,String apellido,String direccion,Date cumpleaños,String email,String numeroTelefono,String password){

        this.nombre=nombre;
        this.apellido=apellido;
        this.direccion=direccion;
        this.cumpleaños=cumpleaños;
        this.email=email;
        this.numeroTelefono=numeroTelefono;
        this.fechaCreacionCuenta= LocalDateTime.now();
        this.password=password;

    }
}
